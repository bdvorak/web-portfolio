import React from "react";

import Footer from "./Footer";
import Bio from "./Bio";
import BioBackground from "./BioBackground";

const IndexPage = () => {
  return (
    <div className="index-page">
      <BioBackground />
      <Bio />
      <Footer />
    </div>
  );
};

export default IndexPage;
