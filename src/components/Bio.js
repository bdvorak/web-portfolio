import React from "react";

const Bio = ({ className }) => (
  <div className={className}>
    <div className="row bio-row">
      <div className="col-sm-3" />
      <div className="bio col-sm-6">
        Ben is a photographer, writer, musician, and programmer based out of
        Stanford, California.
      </div>
    </div>
  </div>
);

export default Bio;
