import React from "react";
import { Link } from "react-router-dom";
// import CSSTransitionGroup from 'react-addons-css-transition-group';

import { routeStrings } from "../constants/navigationConstants";

// renders a photo that links to its own page
const Photo = ({
  className,
  id,
  name,
  src,
  location,
  bw,
  orientation,
  type,
  onLoad
}) => {
  let { singleStr, bwStr, colorStr, photographyStr } = routeStrings;
  let link = `${photographyStr}/${bw ? bwStr : colorStr}/${singleStr}/${id}`;
  return (
    <div className={`${orientation}`}>
      <Link to={link}>
        <img
          src={src}
          alt={name}
          className={className}
        />
      </Link>
    </div>
  );
};

export default Photo;
