import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import IndexPage from "./IndexPage";
import PhotoMain from "./PhotoMain";
import NotFoundPage from "./NotFoundPage";

import { routeStrings } from "../constants/navigationConstants";

// determines the pages content based on url
const Routes = () => {
  let { photographyStr, bwFilterStr, bwStr } = routeStrings;
  return (
    <div>
      <Switch>
        <Route exact path="/" component={IndexPage} />
        <Route
          path={photographyStr + "/:" + bwFilterStr}
          component={PhotoMain}
        />
        <Route
          exact
          path={photographyStr}
          render={() => <Redirect to={photographyStr + "/" + bwStr} />}
        />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  );
};

export default Routes;
