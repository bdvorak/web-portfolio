import React from "react";
import { NavLink } from "react-router-dom";

import { routeStrings } from "../constants/navigationConstants";

// buttons that filter the photography grid
const PhotoFilter = () => {
  let { photographyStr, bwStr, colorStr } = routeStrings;
  return (
    <ul className="nav-list photo-filters">
      <li className="item">
        <NavLink to={photographyStr + "/" + bwStr} activeClassName="active">
          Black & White
        </NavLink>
      </li>
      <li className="item">
        <NavLink
          className="color"
          to={photographyStr + "/" + colorStr}
          activeClassName="active"
        >
          Color
        </NavLink>
      </li>
    </ul>
  );
};

export default PhotoFilter;
