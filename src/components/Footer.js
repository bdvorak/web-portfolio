import React from "react";

// footer at bottom of index page
const Footer = () => {
  return (
    <div className="footer">
      <div className="source-code">
        Made with React.{" "}
        <a href="https://bitbucket.org/bdvorak/web-portfolio/">Source code.</a>
      </div>
    </div>
  );
};

export default Footer;
