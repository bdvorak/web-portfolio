import React from "react";
import Photo from "./Photo";
import Masonry from "react-masonry-css";

import PhotoFilter from "./PhotoFilter";

const PhotoGrid = ({ photos }) => {
  // set behavior of masonry
  let breakpointColumns = {
    default: 3,
    900: 2,
    400: 1
  };
  return (
    <div>
      <PhotoFilter />
      <Masonry
        breakpointCols={breakpointColumns}
        className="my-masonry-grid"
        columnClassName="my-masonry-grid_column"
      >
        {photos.map(photo => {
          return (
            <div className="grid-item" key={photo.id}>
              <Photo {...photo} className="grid-photo" />
            </div>
          );
        })}
      </Masonry>
    </div>
  );
};

export default PhotoGrid;
