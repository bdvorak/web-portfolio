import React from "react";
import { Link } from "react-router-dom";

import { routeStrings } from "../constants/navigationConstants";

// next and previous buttons for image carousel
const CarouselLinks = ({ nextId, prevId, bwfilter }) => {
  let { singleStr, photographyStr } = routeStrings;
  return (
    <div className="carousel-links">
      {/* if id is null then there isn't a previous/next image */}
      {prevId !== null ? (
        <span>
          <Link to={`${photographyStr}/${bwfilter}/${singleStr}/${prevId}`}>
            Previous
          </Link>
          &nbsp;|&nbsp;
        </span>
      ) : (
        <span>
          <span className="inactive">Previous</span> |{" "}
        </span>
      )}
      {nextId !== null ? (
        <Link to={`${photographyStr}/${bwfilter}/${singleStr}/${nextId}`}>
          Next
        </Link>
      ) : (
        <span className="inactive">Next</span>
      )}
    </div>
  );
};

export default CarouselLinks;
