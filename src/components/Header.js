import React from "react";
import { Link } from "react-router-dom";

import { navPages } from "../constants/navigationConstants";

import Navigation from "./Navigation";

// header at top of every page of app
const Header = () => {
  return (
    <div className="header">
      <Navigation pages={navPages} />
      <Link className="name" to={navPages.Home.route}>
        Ben Dvorak
      </Link>
    </div>
  );
};

export default Header;
