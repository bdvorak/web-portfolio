import React from "react";
import { NavLink } from "react-router-dom";

// main navigation panel in header
const Navigation = ({ pages }) => {
  return (
    <ul className="nav-list nav-list-main">
      {Object.keys(pages).map((pageName, i) => (
        <li key={i} className="item">
          <NavLink
            to={pages[pageName].route}
            exact={pages[pageName].exact}
            activeClassName="active"
          >
            {pageName}
          </NavLink>
        </li>
      ))}
    </ul>
  );
};

export default Navigation;
