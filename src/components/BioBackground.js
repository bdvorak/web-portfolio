import React from "react";

import Bio from "./Bio";

const BioBackground = () => (
  <div>
    <div className="background-color">
      <Bio className="invisible" />
    </div>
    <div className="index-background">
      <Bio className="invisible" />
    </div>
  </div>
);

export default BioBackground;
