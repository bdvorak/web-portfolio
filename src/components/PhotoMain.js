import React from "react";
import { Route } from "react-router-dom";

import PhotoGrid from "./PhotoGrid";
import SinglePhoto from "./SinglePhoto";

import photos from "../data/photos";
import { routeStrings } from "../constants/navigationConstants";

// filters photos according to black and white filter
// and sorts by id
const filterAndSortPhotos = (photos, params, bwStr, bwFilterStr) => {
  // if no specified bwfilter, defaults to black and white
  let bwFilter =
    Object.keys(params).length === 0 && params.constructor === Object
      ? bwStr
      : params[bwFilterStr];
  let filterPhotos = photos.filter(photo => {
    return photo.bw === (bwFilter === bwStr);
  });
  filterPhotos.sort((a, b) => a.id - b.id);
  return filterPhotos;
};

// entry point for all photography related pages on the site
const PhotoMain = ({ match }) => {
  let {
    singleStr,
    bwStr,
    colorStr,
    photographyStr,
    photoIdFilterStr,
    bwFilterStr
  } = routeStrings;
  let { params } = match;
  let filteredPhotos = filterAndSortPhotos(photos, params, bwStr, bwFilterStr);
  return (
    <div className="left-right-padding">
      <Route
        path={`${photographyStr}/${bwStr}/${singleStr}/:${photoIdFilterStr}`}
        render={props => <SinglePhoto {...props} photos={filteredPhotos} />}
      />
      <Route
        path={`${photographyStr}/${colorStr}/${singleStr}/:${photoIdFilterStr}`}
        render={props => <SinglePhoto {...props} photos={filteredPhotos} />}
      />
      <Route
        exact
        path={photographyStr}
        render={props => <PhotoGrid {...props} photos={filteredPhotos} />}
      />
      <Route
        exact
        path={photographyStr + "/" + bwStr}
        render={props => <PhotoGrid {...props} photos={filteredPhotos} />}
      />
      <Route
        exact
        path={photographyStr + "/" + colorStr}
        render={props => <PhotoGrid {...props} photos={filteredPhotos} />}
      />
    </div>
  );
};

export default PhotoMain;
