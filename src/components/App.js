import React from "react";
import { HashRouter } from "react-router-dom";

import "../../public/favicon.ico";

import Header from "./Header";
import Routes from "./Routes";

// entry point of React app
const App = () => {
  return (
    // uses HashRouter since we don't need server side rendering
    // and it supports legacy browsers
    <HashRouter>
      <div className="app container-fluid">
        <Header />
        <div className="content">
          <Routes />
        </div>
      </div>
    </HashRouter>
  );
};

export default App;
