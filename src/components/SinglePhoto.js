import React from "react";
import { Redirect } from "react-router-dom";

import Photo from "./Photo";
import NotFoundPage from "./NotFoundPage";
import CarouselLinks from "./CarouselLinks";

import { routeStrings } from "../constants/navigationConstants";

const SinglePhoto = ({ match, photos }) => {
  let { bwStr, colorStr, notFound } = routeStrings;
  let photo = photos.filter(p => p.id === match.params.photoId)[0];
  // if photo is undefined then it does not exist
  if (typeof photo === "undefined") {
    return <Redirect to={"/" + notFound} />;
  }
  let index = photos.indexOf(photo);
  let bwfilter = photo.bw ? bwStr : colorStr;
  // get ids of prev and next photos if they exist
  let prevId = index - 1 >= 0 ? photos[index - 1].id : null;
  let nextId = index + 1 < photos.length ? photos[index + 1].id : null;
  return (
    <div>
      <CarouselLinks prevId={prevId} nextId={nextId} bwfilter={bwfilter} />
      <br />
      <div className="single-photo">
        <Photo {...photo} className="single" />
      </div>
    </div>
  );
};

export default SinglePhoto;
