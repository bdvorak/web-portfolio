const photos = [
  {
    id: "1",
    bw: true,
    name: "Vaillancourt Fountain",
    location: "San Francisco",
    type: "35",
    orientation: "portrait",
    src:
      "https://so4vbg.bn1304.livefilestore.com/y4mQAJvfbzrk4QLykAHym3z47-6rQ2k8d4pWmuibmrFaDODhvdW5cs3DJEvLgl5VpIknpLb3T9hjpesJTmuUucSGqkFP5j0OoObsM7A7w_5ERkVLHvXc6CO1pF3Pp24_5pyypbNEIWTlMVpifUxhwHIMhLZYOs2mRYWJP_FoPB0yOeaemqFwB2o-EhkX424blglUGAu3nOs9eWnjGnRWZFKSw?width=677&height=1024&cropmode=none",
    instalink: "https://www.instagram.com/p/BU_GpO5F3Mj/"
  },
  {
    id: "3",
    bw: true,
    name: "SFFD",
    location: "San Francisco",
    type: "35",
    orientation: "portrait",
    src:
      "https://sy4sbg.bn1304.livefilestore.com/y4mG5Ze7XbacmgJNBWoahDFt9eUfISJ8AAN1RC4QqO5Vkaw7JWYytihSxEydVjQ8FucE2selmbehy7Ybm-ykcZ1zrgC5qQOazs20gqjnuSWaXf2RUNET4ZvV_t0U9leX2KQhVJI2UevlQJ_1btQw3b6JpWu5leOJuLRiH3FT5qSVQPDtRgOB6y5DclDotWyzwWrwGWIzZBhdopWxlb1AC2Q5g?width=685&height=1024&cropmode=none",
    instalink: "https://www.instagram.com/p/BVsN5LVhTH_/"
  },
  {
    id: "2",
    bw: true,
    name: "Corner Building",
    location: "San Francisco",
    type: "35",
    orientation: "landscape",
    src:
      "https://sy4xbg.bn1304.livefilestore.com/y4m731wH1ADsNE9lfiWmEDUuuLcfcaRCBR2KL1lp6oHbe92S_7QujnW4eLo57GNAFl9bGie1fzdEsQ6XuMI3UC3tVtctYS_cnOrM_Jj6Se1TLlBZqqcjOlaZTqDOwb6k_J44a_SnQgWghHqpWRl5smdfOe9QzN_IJrq0W8y_p5qYtT9nT28aJWWurhegzhh4MA-56sCwY1t4kCNAk6CCuQ9fw?width=1024&height=700&cropmode=none",
    instalink: "https://www.instagram.com/p/BVsN5LVhTH_/"
  },
  {
    id: "4",
    bw: true,
    name: "Hospital",
    type: "35",
    orientation: "landscape",
    location: "San Francisco",
    src:
      "https://sy4ubg.bn1304.livefilestore.com/y4mzduLUa_NM-bQgH1MYAwS4AaMFji5Xty3-oKi6ryUZx9JqU9rcRdlH7sm4JvA6TuFZDCTR-056F-nI_B6wTaN4pA0bnxqke-GDMZWCzNhcolvOLBAEyhY7JldHlcX3kOLrbcaEDGhpGJpNoM51WZm7-gPK5R_y3iOOBgAGLlH70ournKpXEFDMoLi2hM7ASpwwb3q4iR0JGkzIJ-rOHa5VA?width=1024&height=674&cropmode=none",
    instalink: "https://www.instagram.com/p/BVGUGnMh3uY/"
  },
  {
    id: "5",
    bw: true,
    name: "McMurtry Bench",
    type: "35",
    orientation: "landscape",
    location: "Stanford",
    src:
      "https://uo7i4g.bn1304.livefilestore.com/y4mvdxV6Nxuvs4i_a34gHGMF9rctIBft7w--t_Mffho3PXrIgdrBAP34pE0vFshou_lR6H8D3qDluo2YiIIb48tUW7TFZn8Say7vUTvdryr_46yQML2FAagSOK5yRWPI_t5QWuCC9olVGtDeHLNoi3itge8v6vrQceSipBh7ePfxJ0Nan20PCraLD8h67JVuXxsj__dKQ-23YCisQTN0VmGVw?width=1024&height=675&cropmode=none",
    instalink: "https://www.instagram.com/p/BWkI4Qhh5zT/"
  },
  {
    id: "6",
    bw: true,
    name: "Playset",
    type: "35",
    orientation: "landscape",
    location: "San Francisco",
    src:
      "https://sy4qbg.bn1304.livefilestore.com/y4moIbKTJyyRZ57gjM-06FJxykP1eQkioStmU5s4oFhlatcoNsGn--tpjjuwJvMqOHuvHplX_PUJjLFnOm4HpWjgK1spHI2SxNgGel2lZPinzjV4HYM1_pwwR0vKIlxZxFtjFt3wGM3GCZbeHR860m-FfMVVY-OQNvZJKMaMg18eExATrYTyHhciaxVUywS3Ltm_zaNce4-BUNtg5wiM-s0vw?width=1024&height=677&cropmode=none",
    instalink: null
  },
  {
    id: "8",
    bw: true,
    name: "Books",
    type: "35",
    orientation: "portrait",
    location: null,
    src:
      "https://sy4lbg.bn1304.livefilestore.com/y4m_dB2sk9D4PBJzx9KvqWnIIP9xI1fW6OSl_j7B62J6Kufxs0pcy_po3aojKMANBlVk6yuav6LNiZ-Pg6795khQjSiUvOmS2Nt66tAjXkrC1Tz6-Z2TzJu3qTUb6i0lm1X4H6Gb0jo0b3MAcHLwOscmn3uAjnajscgqASHO9mxsfL1dNqJZKX2uKi7FiITeq6SXgJGNiMpsgUhN3qJTyABlQ?width=671&height=1024&cropmode=none",
    instalink: null
  },
  {
    id: "7",
    bw: true,
    name: "McMurtry",
    type: "35",
    orientation: "landscape",
    location: "Stanford",
    src:
      "https://uo7h4g.bn1304.livefilestore.com/y4m3UQSSJ6Rd9A3nWmVSY03rhMjsiCnMRV5H5bIUsbgTJPoAMcd66MPQzx1vZ36M8KUeUijJH83N9szlMiOaKQ0sO4NixTgIFYB07U7Ur6rVmcDeauPe_j2OLVe3McgbFxaTQuSpNKj1QaVyb4qUHv7o6cdISL0Yb48OBefc4AZRXBYkOXEE0JodhlNQ8qVexZQODnJahW69O7f2OEOaxCt-A?width=1024&height=672&cropmode=none",
    instalink: null
  },
  {
    id: "9",
    bw: true,
    name: "Spider",
    type: "medium",
    orientation: "portrait",
    location: "Tokyo",
    src:
      "https://tqnhcq.bn.files.1drv.com/y4mkS5xHZCjX5Y7bRHl2S0i5yEz6tJlydRgOuuYmXex4KFdGwQ7V_JRN6P4BHPt4g3IajZLJicujnuqA-Tg3ayBqLuoJpV0xrGV-PLq8-vDpwt9vdqXaEqQV0V5jnTNzb1rPwFvt5iKWHR35qzofjy4ktBai6aH8Y0w2IqHeagmhy34QhqLOv4cpZLsXTyh2mAe8HwC-79jiQYxtCYDnsxfzw?width=836&height=1024&cropmode=none",
    instalink:
      "https://www.instagram.com/p/BXZL93nhJw_4I9l9Ln4W9q87xvsxWwWjYtR5Bs0/"
  },
  {
    id: "10",
    bw: false,
    name: "Olympic",
    type: "medium",
    orientation: "portrait",
    location: "Tokyo",
    src:
      "https://tanocq.bn.files.1drv.com/y4mYCehWxJcgKMffYWWmzZZRRhT-qRmdOEa7dHgy5YTt3lFE676tKQZlxD2R0QhMCRnuAy5v6tvuDzHOy2m8PuI9B-_-zeDU6MB7-MaMMIYI-kKBrtbkGThJ0fRUXlsQH6MJb0ybW7kpQuR21_mZUxhZT0UaG3lm4FLMa0aQ_VnNpIcEu65bNs3OGIvMTStqijWU8JD60eRxJ5WRnpcy4DjiA?width=836&height=1024&cropmode=none",
    instalink:
      "https://www.instagram.com/p/BXEn5TYBLfE4YK_DKsWrivza1wZ3_rbnYe603s0/"
  },
  {
    id: "11",
    bw: true,
    name: "Buildings",
    type: "medium",
    orientation: "landscape",
    location: "Tokyo",
    src:
      "https://kgfrpw.bn.files.1drv.com/y4mNMQYMJuY_aOLsWotRKPpMoSUIFsXtnW1_4WaG2pPXdY7ccWukCxLLezJ8IL1lXS4Sbdrkaf1Y7yjm_8y43QFvqMdwsSOcNFRkWu9nmfidLadooUxvuxJhJrA3DaG1PUF49pe4Eio0YGQs4Gx9KSaO1uEJ25b4_mu9bA8rjq5m_7BGeKGKV5o9Z9nRVrYx6KNj3QkydEQnUovsTCT2mRrGg?width=1024&height=836&cropmode=none",
    instalink:
      "https://www.instagram.com/p/BcdvX_IBgSzjZm-i5YiP6t713YDl9AUbYxLQ9k0/"
  },
  {
    id: "12",
    bw: false,
    name: "Billboard",
    type: "medium",
    orientation: "portrait",
    location: "Tokyo",
    src:
      "https://kgfspw.bn.files.1drv.com/y4mry_203GCAPXiBxCJ-krynV1GLjXyrIycNPF1hzQ36b40Z8MJzZf3-dpKYE97we9NcYIBpvuJVopJJ5JgkmGEksasQVmiMaqtb1cKDTa0ONy-Kde9v_4QY4xY9hujvQtQw4eDhtJDNGATKN3Tsodcp11znmPBCjTekfMB2kTYV5n-xb0iIvfJSWe-DcoDPrM2a2hZFm20UBonjC2VFXfTjQ?width=878&height=1024&cropmode=none",
    instalink:
      "https://www.instagram.com/p/BdlWY3yBb2iwbCxIR1LO90SvZf6jkzj4uzlOpY0/"
  },
  {
    id: "15",
    bw: false,
    name: "School",
    type: "medium",
    orientation: "landscape",
    location: "Tokyo",
    src:
      "https://kgftpw.bn.files.1drv.com/y4mGBwHwaDekbASgTAlpmiQbA9DbC4JXVd_zWTxHmRDLwvc0NBg2KFknvb0ovXYdx3I_0G5BWGQfw2mXAmD-V48_zE8LqccgfTiXNwUcL99kDjAKSbQ-nZoO4N5I97aGfF1OPJgEq6jOK3ft9KiI5blfV_NWmUqdRQ319QUx8CzzaWUh4HgwT24wnnZqiz60nlIofqs6mVuyP_zjvEwaHGKmw?width=1024&height=878&cropmode=none",
    instalink: null
  },
  {
    id: "13",
    bw: false,
    name: "Laundry",
    type: "medium",
    orientation: "portrait",
    location: "Tokyo",
    src:
      "https://kgfypw.bn.files.1drv.com/y4mDvtT74HXvDSZpQnM_JvrEWaSsUOOxdGUJ4DmYTbJ9C697_AoMwmduxgraWs6NZZngvLTxQ31YwB4wCHqpCJVlt6nHA_IE1f06UYdivt4sku1wsEVfeVUXHG_PV8gMW-v0JRPBkv8qWQTC-yjgWUug2v2Mk8dCEzM0uhn6h0ni3ov8T8zDG5wIzXdAP10vjvIo9w_naSa4BNE6sO7cT1lvw?width=835&height=1024&cropmode=none",
    instalink:
      "https://www.instagram.com/p/BZM_l_ghiJP-BtXUOlO4aVCQzXv7etQinNMzto0/"
  },
  {
    id: "14",
    bw: false,
    name: "Wall",
    type: "medium",
    orientation: "portrait",
    location: "Tokyo",
    src:
      "https://kgfxpw.bn.files.1drv.com/y4mWCzf09T8400q3ZB76QXjWocYsZ7RxKKh8xq9D6fUQ28zO_xN4D39nSX-7oM3ob43bAphL5zQRruEODWPWAWWshqTKumDZyTcq73Fjj6kytU_oSPQU6hR5rIqwiXvnfhGkf_lRwlc5vD7dpGwR4pK3srDimgrO0g5oNNKPwoEDbaOEd2I0fCTs9R3-gL7lDWpB39Eu4Azc1KhJreCYTvETA?width=835&height=1024&cropmode=none",
    instalink:
      "https://www.instagram.com/p/BaX1oYNhvpiUtYYcI2oYAhzEQJdQ1cmB-5LM3c0/"
  }
];

export default photos;
