// starting point of React portion of the app

import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
// loads in styles for the app
import "./static/style.scss";

ReactDOM.render(<App />, document.getElementById("root"));
