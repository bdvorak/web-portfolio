export const navPages = {
  Home: {
    exact: true,
    route: "/"
  },
  Photography: {
    route: "/photography/",
    exact: false
  }
  // CV: "/cv"
};

export const routeStrings = {
  singleStr: "single",
  bwStr: "bw",
  colorStr: "color",
  photographyStr: "/photography",
  photoIdFilterStr: "photoId",
  bwFilterStr: "bwFilter",
  notFound: "notfound"
};
