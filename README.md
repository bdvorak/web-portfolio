# Ben Dvorak Online Portfolio

Created with React and Webpack.

To set up, clone repository, enter the project folder, and use `npm install` to install necessary packages. `npm start` will begin a test server. `npm run build` will create a production build in the `dist` folder.

Code formatted with opinionated code formatter Prettier.
